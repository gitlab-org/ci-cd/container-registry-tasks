# Container Registry Group Tasks

This is a project for the [Container Registry Group](https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/) to hold team process discussions and efforts towards team continuity and productivity. Generally, a place to have more structured discussions around things we want to do.